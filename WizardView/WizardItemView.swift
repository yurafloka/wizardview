//
//  CustomView.swift
//  WizardView
//
//  Created by Yurii Tsymbala on 9/18/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class WizardItemView: UIView {
  @IBOutlet private weak var itemLabel: UILabel!
  @IBOutlet private weak var backgroundItemView: UIView!
  @IBOutlet private weak var infoImageView: UIImageView!
  @IBOutlet private weak var infoLabel: UILabel!
  @IBOutlet private weak var infoStackView: UIStackView!
  @IBOutlet private weak var globalStackView: UIStackView!

  var viewModel: CustomViewViewModel!

  var itemText = "" {
    didSet {
      itemLabel.text = itemText
    }
  }
  var infoImage: UIImage? {
    didSet {
      infoImageView.contentMode = .scaleAspectFill
      infoImageView.image = infoImage
    }
  }
  var infoText = "" {
    didSet {
      infoLabel.text = infoText
    }
  }
  var infoStackViewIsHidden = false {
    didSet {
      infoStackView.isHidden = infoStackViewIsHidden
    }
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    let _ = loadFromNib()
    setupView()
  }

  func bind(withViewModel viewModel: CustomViewViewModel) {
    self.viewModel = viewModel
    itemText = self.viewModel.itemText
    infoImage = self.viewModel.infoImage
    infoText = self.viewModel.infoText
    infoStackViewIsHidden = self.viewModel.infoStackViewIsHidden
  }

  @objc private func userTapped() {
    viewModel.gestureCallBack?()
  }

  private func setupView() {
    setupGesture()
    itemLabel.backgroundColor = .red
  }

  private func setupGesture() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTapped))
    tapGesture.numberOfTapsRequired = 1
    infoStackView.addGestureRecognizer(tapGesture)
  }

  private func loadFromNib() -> UIView {
    let bundle = Bundle.init(for: type(of: self))
    let nib = UINib(nibName: "WizardItemView", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
    addSubview(view)
    return view
  }
}
