//
//  ShapeCollectionViewCell.swift
//  WizardView
//
//  Created by Yurii Tsymbala on 9/18/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class ShapeCollectionViewCell: UICollectionViewCell {
  @IBOutlet  weak var imgShape: UIImageView!

  override func awakeFromNib() {
    super.awakeFromNib()
    imgShape.contentMode = .scaleAspectFill
    imgShape.image = #imageLiteral(resourceName: "little-star-icon-74127")
  }
}
