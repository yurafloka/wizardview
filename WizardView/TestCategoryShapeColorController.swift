//
//  ViewController.swift
//  WizardView
//
//  Created by Yurii Tsymbala on 9/17/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class TestCategoryShapeColorController: UIViewController {

  /* 1 scroll view {
   1 stack view {
   1) CategoryCustomView
   2) CategoryTableView
   3) ShapeCustomView
   4) ShapeCollectionView
   5) ColorCustomView
   6) ColorCollectionView
   7) MedicationCustomView
   8) MedicationDailyView
   }
   }
   */


  @IBOutlet private weak var scrollView: UIScrollView!

  @IBOutlet private weak var mainStackView: UIStackView!

  @IBOutlet private weak var categoryCustomView: WizardItemView!
  @IBOutlet private weak var categoryTableView: UITableView!
  @IBOutlet private weak var categoryTableViewHeight: NSLayoutConstraint!

  @IBOutlet private weak var shapeCustomView: WizardItemView!
  @IBOutlet private weak var shapeCollectionView: UICollectionView!
  @IBOutlet private weak var shapeCollectionViewHeight: NSLayoutConstraint!

  override func viewDidLoad() {
    super.viewDidLoad()
    categorySetup()
    shapeSetup()
    observeViewModel()
  }

  private func observeViewModel() {
    categoryCustomView.viewModel.gestureCallBack = { [weak self] in
      self?.showCategoryTableView()
    }
    shapeCustomView.viewModel.gestureCallBack = { [weak self] in
      self?.showShapeCollectionView()
    }
  }

  private func categorySetup() {
    categoryCustomView.bind(withViewModel: CustomViewViewModel(withItemText: "Dosage Form",
                                                               withinfoImage: #imageLiteral(resourceName: "icon_temp"),
                                                               withinfoText: "Pills, Tablets or Capsules",
                                                               withinfoStackViewIsHidden: false))
    categoryTableViewSetup()
  }

  private func shapeSetup() {
    shapeCustomView.bind(withViewModel: CustomViewViewModel(withItemText: "Great job! Just a bit more to go." +
      "Selecting a shape will help your recognize your medication in the home screen",
                                                            withinfoImage: #imageLiteral(resourceName: "little-star-icon-74127"),
                                                            withinfoText: "The Do-not-enter sign",
                                                            withinfoStackViewIsHidden: true))
    shapeCollectionViewHeight.constant = 300
    shapeCollectionViewSetup()
  }

  private func categoryTableViewSetup() {
    categoryTableView.delegate = self
    categoryTableView.dataSource = self
    categoryTableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil),
                               forCellReuseIdentifier: "CategoryTableViewCell")
    categoryTableView.isHidden = true
  }

  private func shapeCollectionViewSetup() {
    shapeCollectionView.delegate = self
    shapeCollectionView.dataSource = self
    shapeCollectionView.register(UINib(nibName: "ShapeCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: "ShapeCollectionViewCell")
    shapeCollectionView.isHidden = false
  }

  private func showCategoryTableView() {
    categoryTableViewHeight.constant = 300
    categoryCustomView.itemText = "Please Select the category of your therapy. This will help us calculate refills"
    shapeCustomView.itemText = "Shape Of Pill"
    categoryTableView.isHidden = false
    shapeCollectionView.isHidden = true
    categoryCustomView.infoStackViewIsHidden = true
    shapeCustomView.infoStackViewIsHidden = false
  }

  private func showShapeCollectionView() {
    shapeCustomView.itemText = "Great job! Just a bit more to go. Selecting a shape will help your recognize your medication in the home screen"
    categoryCustomView.itemText = "Dosage Form"
    categoryTableView.isHidden = true
    shapeCollectionView.isHidden = false
    categoryCustomView.infoStackViewIsHidden = false
    shapeCustomView.infoStackViewIsHidden = true
  }

  private func hideCategoryTableView() {
    categoryCustomView.itemText = "Dosage Form"
    categoryTableView.isHidden = true
    categoryCustomView.infoStackViewIsHidden = false
  }

  private func hideShapeCollectionView() {
    shapeCustomView.itemText = "Shape Of Pill"
    shapeCollectionView.isHidden = true
    shapeCustomView.infoStackViewIsHidden = false
  }
}

extension TestCategoryShapeColorController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = categoryTableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell",
                                                     for: indexPath) as? CategoryTableViewCell
    return cell!
  }
}

extension TestCategoryShapeColorController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    hideCategoryTableView()
  }
}

extension TestCategoryShapeColorController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 15
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = shapeCollectionView.dequeueReusableCell(withReuseIdentifier: "ShapeCollectionViewCell",
                                                       for: indexPath) as? ShapeCollectionViewCell
    return cell!
  }
}
extension TestCategoryShapeColorController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    hideShapeCollectionView()
  }
}

