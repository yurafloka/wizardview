//
//  CategoryTableViewCell.swift
//  WizardView
//
//  Created by Yurii Tsymbala on 9/18/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
  @IBOutlet weak var categoryImageView: UIImageView!
  @IBOutlet weak var categoryLabel: UILabel!

  override func awakeFromNib() {
        super.awakeFromNib()
        categoryImageView.contentMode = .scaleAspectFill
        categoryImageView.image = #imageLiteral(resourceName: "icon_temp")
        categoryLabel.text = "Pills, Tablets or Capsules"
    }


}
