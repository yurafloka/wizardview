//
//  CustomViewViewModel.swift
//  WizardView
//
//  Created by Yurii Tsymbala on 9/20/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class CustomViewViewModel {
  var itemText: String
  var infoImage: UIImage
  var infoText: String
  var infoStackViewIsHidden: Bool

  var gestureCallBack : (()-> Void)?

  init(withItemText itemText: String,
       withinfoImage infoImage: UIImage,
       withinfoText infoText: String,
       withinfoStackViewIsHidden infoStackViewIsHidden: Bool) {
    self.itemText = itemText
    self.infoImage = infoImage
    self.infoText = infoText
    self.infoStackViewIsHidden = infoStackViewIsHidden
  }
}
